**This project contains the master files for the Tree of Life ID (ToLID) sample names developed by the Darwin Tree of Life project (DToL, https://www.darwintreeoflife.org/) for use by the Earth BioGenome Project (EBP, https://www.earthbiogenome.org/).**

They consist of
- a master list that contains the assignments of ToLID prefixes to species. It is not assumed to be comprehensive: tolids.txt
- a list of clade prefix assignments: prefix_assignment.xlsx
- an additional list of clade prefixes tailored to taxonomyDB entries: phylum.txt



**ToLIDs**

A complete ToLID is a unique identifier for an individual sampled and consists of 

1) the ToLID prefix as specified in tolids.txt which is made up of 
- a lower case letter for the high level taxonomic rank and a lower case letter for the clade (see clade prefix assignments). Only one letter is used for vertebrates (VGP legacy).
- one upper, two lower case letters for genus 
- one upper, three lower case letters for species (one upper, two lower case for vertebrates, VGP legacy)

2) a number to indicate the individual that was sampled. The number is assigned in order of request and does not represent any ranking.

=> e.g. aRanTem1 for the first sampled individual of Rana temporaria, xgPerPere3 for the third sampled individual of Peregriana peregra


**Examples**


The tolids.txt file contains assignments of ToLID prefixes (1) for all species expected to be sequenced by DToL and those already in INSDC. Not all species known to taxonomyDB are represented and the taxonomic assignments might be controversial, but a starting point to pre-empt duplication issues. Errors are corrected were identified and new species added where necessary. 

Examples:
<ToLID prefix, species, taxID, common name, genus, family, order, class, phylum (the classification is and stays controversial)>
- xgDanTine  Danilia tinei None	None Danilia	Chilodontidae  Seguenziida Gastropoda	Mollusca
- fSalSal Salmo salar	8030 Atlantic salmon	Salmo Salmonidae	Salmoniformes  Actinopterygii Chordata

For naming genome assemblies of samples, we recommend to use the full ToLID and add .\<version\>

Examples:
- fCotGob3.1 (first assembly version of the 3rd individual of Cottoperca gobio)
- fAstCal1.2 (second assembly version of the first individual of Astatotilapia calliptera) 


**Requests**


If you would like to request a species to be added to the list of ToLID prefixess, please email kj2[at]sanger.ac.uk providing species name and taxonomy ID.

For requesting full ToLID assignments to samples, we will be providing a self-service repository soon. Until then, please email kj2[at]sanger.ac.uk in case of urgent requests providing the species name, the taxon ID and your own identifiers for each sample.
 

**Updates**

09.12.2020: The data has been adapted to make it suitable for use by the EBP. This includes 
- the renaming of final_merged.txt to tolids.txt
- the retiring of previous lists (master_species_list.txt)
- the retiring of the assign.pl script as this is now managed by a repository (details for naming requests to follow)
- the retiring of the assignments list unique_assigned_ids.txt as these are now managed in a repository (details for naming requests to follow)

27.07.2020: The master_species_list has been replaced with a list that contains the Darwin master entries plus the entries in taxonomyDB from this month. The script is now checking against this new "final_merged" list.


**Author**


Kerstin Howe, Tree of Life Programme, Wellcome Sanger Institute, UK, 10.12.2020
